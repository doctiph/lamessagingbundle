<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 28/03/14
 * Time: 09:11
 */

namespace La\MessagingBundle\Model;


interface MessageWrapperInterface {

    /**
     * Return the destination address (email, phoneNumber or postal_address )
     * @return mixed
     */
    public function getDestination();

    /**
     * Get the context for this message
     * @return mixed
     */
    public function getContext();

    /**
     * Get the type of message wrapped internally
     * @return mixed
     */
    public function getInternalType();

    /**
     * Gets the message wrapped internally
     *
     * @return mixed
     */
    public function getWrappedMessage();
} 