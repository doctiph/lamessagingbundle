<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 28/03/14
 * Time: 09:38
 */

namespace La\MessagingBundle\Model;

/**
 * Class SwiftMailerWrapper
 * @package La\MessagingBundle\Model
 *
 * @method \Swift_Message getWrappedMessage
 */
class SwiftMailerWrapper extends AbstractMessageWrapper
{

    protected $internalType = 'Swift_Message';

    /**
     * Passing parameters to construct the email message for Swift.
     *
     *
     * @param $to
     * @param array $context
     * @param array $parameters
     * @return mixed|void
     * @throws \Exception
     */
    public function createWrapped($to, $context = array(), $parameters = array())
    {
        $this->destination = $to;

        $this->context = $context;

        $message = \Swift_Message::newInstance();

        if (isset($parameters['to'])) {
            if (is_array($parameters['to'])) {
                $destinations = array_merge(array($to => null), $parameters['to']);
            } else {
                $destinations = array($to => $parameters['to']);
            }

            $message->setTo($destinations);
        } else {
            $message->setTo($to);
        }

        if(isset($parameters['from']))
        {
            if(is_array($parameters['from']) && !isset($parameters['sender']))
                throw new \Exception('If you establish the "from" parameter as array, you must specify the "sender" parameter with the real sender address.');
        }

        // adding others
        foreach (array(
                     'cc',
                     'bcc',
                     'from',
                     'returnPath',
                     'maxLineLength',
                     'priority',
                     'date',
                     'replyTo',
                     'contentType',
                     'charset',
                     'sender',
                     'subject'
                 ) as $property) {
            if (isset($parameters[$property])) {
                call_user_func_array(array($message, 'set' . ucfirst($property)), array($parameters[$property]));
            }
        }

        // message body

        if (isset($parameters['body'])) {
            $message->setBody($parameters['body'], 'text/html');
        }

        // adding parts
        if (isset($parameters['parts'])) {
            if (is_array($parameters['parts'])) {
                foreach ($parameters['parts'] as $format => $content) {
                    $message->addPart($content, $format);
                }
            }
        }

        // attachments
        if (isset($parameters['attachments'])) {
            if (is_array($parameters['attachments'])) {
                foreach ($parameters['attachments'] as $adjunt) {
                    $message->attach($adjunt);
                }
            }
        }

        // signing the message with digital sign
        if (isset($parameters['signer'])) {
            $message->attachSigner($parameters['signer']);
        }

        $this->msg = $message;
    }

    public function __toString()
    {
        return $this->destination;
    }

    /**
     * @return \DateTime
     */
    public function getSentDate()
    {
        return (new \DateTime())->setTimestamp($this->getWrappedMessage()->getDate());
    }
}