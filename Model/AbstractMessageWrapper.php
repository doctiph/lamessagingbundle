<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 28/03/14
 * Time: 09:16
 */

namespace La\MessagingBundle\Model;


abstract class AbstractMessageWrapper implements MessageWrapperInterface
{

    /**
     * @var
     */
    protected $msg;

    /**
     * @var array
     */
    protected $context;

    /**
     * @var string
     */
    protected $destination;

    /**
     * @var string
     */
    protected $internalType;


    public function __construct($to, $context = array(), $parameters = array())
    {
        $this->createWrapped($to, $context, $parameters);
    }

    /**
     * Creates the message and wraps it
     *
     * @param $to
     * @param array $context
     * @param array $parameters
     * @return mixed
     */
    public abstract function createWrapped($to, $context = array(), $parameters = array());

    /**
     * @param $msg
     * @throws \Exception
     */
    public function wrap($msg)
    {
        if (!($msg instanceof $this->internalType)) {
            throw new \Exception(sprintf(
                "Wrapper of type %s only supports %s message type,  %s given. ",
                get_class($this),
                $this->internalType,
                get_class($msg)
            ));
        }

        $this->msg = $msg;
    }


    /**
     * Return the destination address (email, phoneNumber or postal_address )
     * @return mixed
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Get the context for this message
     * @return mixed
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Get the type of message wrapped internally
     * @return mixed
     */
    public function getInternalType()
    {
        return $this->internalType;
    }

    /**
     * Gets the message wrapped internally
     *
     * @return mixed
     */
    public function getWrappedMessage()
    {
        return $this->msg;
    }

    /**
     * Get the sent date from a message
     * @return \DateTime
     */
    public abstract  function getSentDate();

}