<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 28/03/14
 * Time: 09:26
 */

namespace La\MessagingBundle\Model;


use La\Lib\Messaging\Message\SmartFocusMessage;

/**
 * Class SmartFocusWrapper
 * @package La\MessagingBundle\Model
 *
 * @method SmartFocusMessage getWrappedMessage
 */
class SmartFocusWrapper extends AbstractMessageWrapper {

    protected $internalType = 'La\Lib\Messaging\Message\SmartFocusMessage';

    /**
     * Creates the message and wraps it
     *
     * @param $to
     * @param array $context
     * @param array $parameters
     * @return mixed
     */
    public function createWrapped($to, $context = array(), $parameters = array())
    {
        $this->destination = $to;

        $this->context = $context;

        $stype = isset($parameters['stype']) ? $parameters['stype'] : null;

        $senddate = isset($parameters['senddate']) ? $parameters['senddate'] : null;

        $uidkey = isset($parameters['uidkey']) ? $parameters['uidkey'] : 'EMAIL';

        $this->wrap(SmartFocusMessage::newInstance($to, $context, $senddate, $stype, $uidkey));
    }

    /**
     * @return \DateTime
     */
    public function getSentDate()
    {
        return $this->getWrappedMessage()->getSenddate();
    }
}