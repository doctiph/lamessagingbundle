<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 13/03/14
 * Time: 11:15
 */

namespace La\MessagingBundle\Factory;


use La\MessagingBundle\Mailer\MailerSender;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Form\Exception\InvalidConfigurationException;

class MailerFactory implements ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function using($mailer_name, EventDispatcher $dispatcher)
    {
        $mailerSender = new MailerSender($this->container->getParameter(
            'la_messaging.templates'
        ), $this->container->get('logger'), $dispatcher);

        // if the service exist, load it directly
        if ($this->container->has(sprintf('la_messaging.adapter.%s', $mailer_name))) {
            $mailerSender->setMailerAdapter(
                $this->container->get(sprintf('la_messaging.adapter.%s', $mailer_name))
            );
        } elseif ($this->container->has($mailer_name)) {
            $mailerSender->setMailerAdapter($this->container->get($mailer_name));

        } elseif (class_exists($mailer_name)) {
            $mailerSender->setMailerAdapter(
                $this->container->setParameter('la_messaging.default_mailer', new $mailer_name)
            );
        } else {
            throw new InvalidConfigurationException(sprintf(
                "The class or service '%s' is not a valid mailer",
                $mailer_name
            ));
        }

        return $mailerSender;
    }

    /**
     * Sets the Container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @api
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}