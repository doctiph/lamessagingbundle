<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 13/03/14
 * Time: 10:23
 */

namespace La\MessagingBundle\Mailer;

use La\MessagingBundle\Adapter\MailerAdapterInterface;
use La\MessagingBundle\DataCollector\MessagingDataCollector;
use La\MessagingBundle\Event\EmailEvent;
use La\MessagingBundle\Event\EmailEvents;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


/**
 * Class MailerSender
 * @package La\MessagingBundle\Mailer
 */
class MailerSender
{

    /**
     * @var MailerAdapterInterface
     */
    protected $mailerAdapter;

    /**
     * @var array<MailerAdapterInterface>
     */
    protected $adapters = array();

    /**
     * @var array
     */
    protected $failures = array();

    /**
     * @var array<MessageWrapperInterface>
     */
    protected $queue = array();

    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    protected $dispatcher;


    protected $collector;

    /**
     * @param array $templates
     * @param EventDispatcherInterface $dispatcher
     * @param MessagingDataCollector $collector
     */
    public function __construct(
        $templates = array(),
        EventDispatcherInterface $dispatcher,
        MessagingDataCollector $collector = null
    ) {
        $this->dispatcher = $dispatcher;

        $this->templates = $templates;
        $this->queue = array();

        $this->collector = $collector;
    }

    /**
     * @param bool $forceEmpty
     * @return int
     * @throws \Exception
     */
    public function flush($forceEmpty = false)
    {
        if (count($this->queue) == 0) {
            throw new \Exception("You can't call flush() method empty queue!");
        }

        $count = $this->send($this->queue, $this->failures);

        // if user force queue to get empty
        if ($forceEmpty) {
            $this->queue = array();
        } else {
            foreach ($this->failures as $key => $value) {
                for ($i = 0; $i < count($this->queue); $i++) {
                    if ($this->queue[$i]->getDestination() != $key && $this->queue[$i]->getDestination() != $value) {
                        array_pop($this->queue);
                        $i--;
                    } else {
                        continue;
                    }
                }
            }
        }

        return $count;
    }

    /**
     * @param array $messages
     * @return int
     */
    public function send(array $messages)
    {
        $this->failures = array();
        $count = 0;

        $queue = array();

        // redistributing per adapter
        foreach ($messages as $msg) {
            foreach ($this->adapters as $adapter) {
                if ($adapter->shouldHandle($msg)) {
                    if (!isset($queue[$adapter->getName()])) {
                        $queue[$adapter->getName()] = array();
                    }

                    $queue[$adapter->getName()][] = $msg;
                }
            }
        }

        // sending per adapter
        foreach ($this->adapters as $adapter) {
            if (isset($queue[$adapter->getName()])) {
                $failures = array();
                $count += $adapter->send($queue[$adapter->getName()], $failures);

                $this->failures = array_merge($this->failures, $failures);

                $this->dispatcher->dispatch(
                    EmailEvents::EMAIL_SENT_WITH_SUCCESS,
                    new EmailEvent($adapter)
                );
            }
        }

        //$this->collector->recollect($messages, $this->failures, $this->adapters);

        return $count;
    }

    /**
     * Create a message and adds it to queue
     *
     * @param string $to
     * @param array $context
     * @param array $parameters
     * @throws \Exception
     */
    public function createAndQueue($to, $context = array(), array $parameters = array())
    {
        if (is_array($to)) {
            throw new \Exception("You must pass a valid email address");
        }

        $this->queue[] = $this->createMessage($to, $context, $parameters);
    }

    /**
     * @param $to
     * @param array $context
     * @param array $parameters
     * @return mixed
     */
    public function createMessage($to, $context = array(), $parameters = array())
    {
        return $this->mailerAdapter->createMessage($to, $context, $parameters);
    }

    /**
     * @param MailerAdapterInterface $adapter
     * @param int $priority
     */
    public function addAdapter(MailerAdapterInterface $adapter, $priority = 0)
    {
        $adapter->setPriority($priority);

        if (isset($this->templates[$adapter->getName()])) {
            $adapter->store($this->templates[$adapter->getName()]);
        }

        $this->adapters[$adapter->getName()] = $adapter;

        // Comparison function
        $cmp = function($adapterA, $adapterB) {
            if ($adapterA->getPriority() == $adapterB->getPriority()) {
                return 0;
            }
            return ($adapterA->getPriority() < $adapterB->getPriority()) ? -1 : 1;
        };

        uasort($this->adapters, $cmp);
    }

    /**
     * Specify the template used to send message!
     *
     * @param $templateName
     * @throws \Exception
     * @return $this
     */
    public function withTemplate($templateName)
    {
        $exists = false;
        $templateNames = array();

        foreach ($this->adapters as $adapter) {
            $templateNames = array_merge(
                $templateNames,
                array_combine(
                    array_keys($this->templates[$adapter->getName()]),
                    array_keys($this->templates[$adapter->getName()])
                )
            );
        }

        if (!in_array($templateName, $templateNames)) {
            throw new \Exception(sprintf(
                "The template \"%s\" is not configured or does not exist. Availables in configuration are: %s",
                $templateName,
                implode(', ', $templateNames)
            ));
        }

        /**
         * if the main adapter has not configured the requested template
         * search the adapter which has this template configured
         */
        if (!$this->getMailerAdapter()->hasTemplate($templateName)) {
            foreach ($this->adapters as $adapter) {
                if ($adapter->getName() !== $this->getMailerAdapter()->getName()
                    and $adapter->hasTemplate($templateName)
                ) {
                    $this->using($adapter->getName());
                    break;
                }

            }
        }

        $this->getMailerAdapter()->useTemplate($templateName);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMailerAdapter()
    {
        return $this->mailerAdapter;
    }

    /**
     * @param MailerAdapterInterface $mailerAdapter
     */
    public function setMailerAdapter(MailerAdapterInterface $mailerAdapter)
    {
        $this->mailerAdapter = $mailerAdapter;
    }

    /**
     * Specify which adapter would you like to use
     *
     * @param $adapter
     * @return $this
     * @throws \Exception
     */
    public function using($adapter)
    {
        if ($adapter instanceof MailerAdapterInterface) {
            $this->mailerAdapter = $adapter;
        } elseif (is_string($adapter) && isset($this->adapters[$adapter])) {
            $this->mailerAdapter = $this->adapters[$adapter];
        } else {
            throw new \Exception(sprintf("You must specify the adapter name or pass un MailerAdapterInterface object"));
        }

        return $this;
    }

    public function getContext()
    {
        return $this->mailerAdapter->getContext();
    }

    /**
     * @param $to
     * @param array $context
     * @param array $parameters
     * @return mixed
     * @throws \Exception
     */
    public function createAndSend($to, $context = array(), array $parameters = array())
    {
        try {
            $failures = array();
            $count = $this->mailerAdapter->createAndSend($to, $context, $parameters, $failures);
            $this->dispatcher->dispatch(
                EmailEvents::EMAIL_SENT_WITH_SUCCESS,
                new EmailEvent($this->getMailerAdapter())
            );

            return $count;
        } catch (\Exception $e) {
            $this->dispatcher->dispatch(
                EmailEvents::EMAIL_SENT_WITH_ERROR,
                new EmailEvent($this->getMailerAdapter(), $e->getMessage())
            );

            throw $e;
        }
    }

    public function __call($method, $args)
    {
        if (!method_exists($this->mailerAdapter->getMailer(), $method)) {
            throw new \Exception(sprintf(
                "Unknown method [%s] in class [%s]",
                $method,
                get_class($this->mailerAdapter)
            ));
        }

        return call_user_func_array(
            array($this->mailerAdapter->getMailer(), $method),
            $args
        );
    }

    /**
     * @return array<MailerAdapterInterface>
     */
    public function getAdapters()
    {
        return $this->adapters;
    }

    /**
     * @return array
     */
    public function getFailures()
    {
        return $this->failures;
    }

    public function getQueue()
    {
        return $this->queue;
    }
}