<?php

namespace La\MessagingBundle\Spool;


/**
 * Class Spool
 * @package La\MessagingBundle\Spool
 */
class Spool
{
    /** The spool directory */
    private $_path;
    /** File write retry limit */
    private $_retryLimit = 10;
    /** The maximum number of messages to send per flush */
    private $_message_limit;
    /** The time limit per flush */
    private $_time_limit;
    /** Boolean : Must flush each time a file is spooled */
    private $_autoflush;

    /**
     * Sets the maximum number of messages to send per flush.
     * @param int $limit
     */
    public function setMessageLimit($limit)
    {
        $this->_message_limit = (int)$limit;
    }

    /**
     * Gets the maximum number of messages to send per flush.
     * @return int     The limit
     */
    public function getMessageLimit()
    {
        return $this->_message_limit;
    }

    /**
     * Sets the time limit (in seconds) per flush.
     * @param int $limit The limit
     */
    public function setTimeLimit($limit)
    {
        $this->_time_limit = (int)$limit;
    }

    /**
     * Gets the time limit (in seconds) per flush.
     * @return int     The limit
     */
    public function getTimeLimit()
    {
        return $this->_time_limit;
    }

    /**
     * Allow to manage the enqueuing retry limit.
     *
     * Default, is ten and allows over 64^20 different fileNames
     *
     * @param int $limit
     */
    public function setRetryLimit($limit)
    {
        $this->_retryLimit = $limit;
    }

    /**
     * Must flush each time a file is spooled ?
     * @return bool
     */
    public function mustAutoflush()
    {
        return $this->_autoflush;
    }


    /**
     * Create a new FileSpool.
     * @param string $path
     * @param bool $autoflush
     * @throws \Exception
     */
    public function __construct($path, $autoflush = false)
    {
        $this->_path = $path;
        if (!file_exists($this->_path)) {
            if (!mkdir($this->_path, 0777, true)) {
                throw new \Exception('Unable to create Path [' . $this->_path . ']');
            }
        }
        $this->_autoflush = $autoflush;
    }

    /**
     * Tests if this Spool mechanism has started.
     * @return bool
     */
    public function isStarted()
    {
        return true;
    }

    /**
     * Starts this Spool mechanism.
     */
    public function start()
    {
    }

    /**
     * Stops this Spool mechanism.
     */
    public function stop()
    {
    }

    /**
     * Queues several messages.
     * @param array $messages
     * @param object $template The messsages template
     * @param array $failures Encountered failures so far
     * @throws \Exception
     * @return int
     */
    public function queueMessages(array $messages, $template, $failures)
    {
        $processed = 0;
        foreach ($messages as $message) {
            $processed += (int)$this->queueMessage($message, $template);
        }
        return $processed;
    }

    /**
     * Queues a message.
     * @param object $message The message to store
     * @param object $template The messsage template
     * @throws \Exception
     * @return bool
     */
    public function queueMessage($message, $template)
    {
        $spooledMsg = array(
            'message' => $message,
            'template' => $template
        );
        $ser = serialize($spooledMsg);
        $fileName = $this->_path . '/' . $this->getRandomString(10);
        for ($i = 0; $i < $this->_retryLimit; ++$i) {
            /* We try an exclusive creation of the file. This is an atomic operation, it avoid locking mechanism */
            $fp = @fopen($fileName . '.message', 'x');
            if (false !== $fp) {
                if (false === fwrite($fp, $ser)) {
                    return false;
                }

                return fclose($fp);
            } else {
                /* The file already exists, we try a longer fileName */
                $fileName .= $this->getRandomString(1);
            }
        }
        throw new \Exception('Unable to create a file for enqueuing Message');
    }

    /**
     * Execute a recovery if for any reason a process is sending for too long.
     * @param int $timeout in second Defaults is for very slow smtp responses
     */
    public function recover($timeout = 900)
    {
        foreach (new \DirectoryIterator($this->_path) as $file) {
            /** @var \SplFileInfo $file */
            $file = $file->getRealPath();

            if (substr($file, -16) == '.message.sending') {
                $lockedTime = filectime($file);
                if ((time() - $lockedTime) > $timeout) {
                    rename($file, substr($file, 0, -8));
                }
            }
        }
    }

    /**
     * Sends messages using the current mailer instance.
     *
     * @return array     The enqueued messages to send
     */
    public function flushQueue()
    {
        $directoryIterator = new \DirectoryIterator($this->_path);

        $messages = [];
        $count = 0;
        $time = time();

        foreach ($directoryIterator as $file) {
            /** @var \SplFileInfo $file */
            $file = $file->getRealPath();

            if (substr($file, -8) != '.message') {
                continue;
            }

            /* We try a rename, it's an atomic operation, and avoid locking the file */
            if (rename($file, $file . '.sending')) {
                $messages[$file] = unserialize(file_get_contents($file . '.sending'));
            } else {
                /* This message has just been catched by another process */
                continue;
            }

            if ($this->getMessageLimit() && $count >= $this->getMessageLimit()) {
                break;
            }
            if ($this->getTimeLimit() && (time() - $time) >= $this->getTimeLimit()) {
                break;
            }

        }

        return $messages;
    }

    /**
     * Returns a random string needed to generate a fileName for the queue.
     *
     * @param int $count
     *
     * @return string
     */
    protected function getRandomString($count)
    {
        // This string MUST stay FS safe, avoid special chars
        $base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-.";
        $ret = '';
        $strlen = strlen($base);
        for ($i = 0; $i < $count; ++$i) {
            $ret .= $base[((int)rand(0, $strlen - 1))];
        }

        return $ret;
    }
}
