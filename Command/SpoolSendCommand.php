<?php
namespace La\MessagingBundle\Command;


use La\Lib\Messaging\Message\SmartFocusMessage;
use La\Lib\Messaging\Mailer\Mailer;
use La\MessagingBundle\Adapter\MailerAdapterInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SpoolSendCommand extends ContainerAwareCommand
{

    protected $templates = array();

    /**
     * @param $service
     * @return object
     */
    public function get($service)
    {
        return $this->getContainer()->get($service);
    }

    protected function configure()
    {
        $this
            ->setName('messaging:spool:send')
            ->setDescription("Send all spooled emails");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var MailerAdapterInterface $adapter */
        $adapter = $this->getContainer()->get(sprintf('la_messaging.adapter.%s', $this->getContainer()->getParameter('la_messaging.default_mailer')));
            try {
                if(method_exists($adapter, 'flushSpool')){
                $output->writeln(sprintf('Processing <info>%s</info> mailer... ', $adapter->getName()));
                $sent = $adapter->flushSpool();
                $output->writeln(sprintf('<comment>%d</comment> emails sent', $sent));
                }else{
                    $output->writeln(sprintf("Spool wasnt configured for adapter %s", $adapter->getName()));
                }
            } catch (\Exception $e) {
                $output->writeln(sprintf('<error>An error occured : %s</error>', $e->getMessage()));
            }

    }
}