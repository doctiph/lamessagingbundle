<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 25/03/14
 * Time: 10:48
 */

namespace La\MessagingBundle\Command;


use La\Lib\Messaging\Configuration\RequestConfiguration;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Dumper\XmlDumper;
use Symfony\Component\Yaml\Yaml;

class RequestConfigurationCommand extends ContainerAwareCommand
{

    protected $templates = array();

    /**
     * @param $service
     * @return object
     */
    public function get($service)
    {
        return $this->getContainer()->get($service);
    }

    protected function configure()
    {
        $this
            ->setName('messaging:smartfocus:create-config')
            ->setDescription("Request templates configuration from smartfocus")
            ->addOption('directory', 'd', InputOption::VALUE_OPTIONAL, null)
            ->addArgument('type', InputArgument::OPTIONAL, 'Template type', 'TRANSACTIONAL')
            ->addArgument('format', InputArgument::OPTIONAL, 'Configuration export format', 'yml');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = $this->getContainer()->getParameter('la_messaging.smartfocus.config');

        $format = $input->getArgument('format');

        $dialog = $this->getHelperSet()->get('dialog');

        $directory = $input->getOption('directory') ?: $this->getContainer()->getParameter('kernel.root_dir').'/config';

        $type = $input->getArgument('type') ?: $dialog->ask(
            $output,
            sprintf("Type of templates: "),
            null
        );


        $report = new RequestConfiguration($config['server'], $config['login'], $config['pwd'], $config['key']);

        $report->openConnection();

        $templates = $report->getConfiguredTemplates($type);

        $report->closeConnection();

        $configured = $this->getContainer()->getParameter('la_messaging.smartfocus.templates');

        $config = array(
            'la_messaging' => array(
                'smartfocus' => array(
                    'templates' => $templates
                )
            )
        );

        $extension = $format;
        $content = null;
        $file = sprintf("%s/smartfocus.%s", $directory, $extension);

        switch ($format) {
            case 'yml':
                $content = Yaml::dump($config, 10);
                break;
            case 'xml':
                $content = (new XmlDumper(null))->phpToXml($config);
                break;
            default:
                $content = $config;
                break;
        }

        $output->writeln(sprintf("generating config file into %s", $directory));
        file_put_contents($file, $content);
        $output->writeln(sprintf("Config file generated successfully"));

        $output->writeln(
            sprintf(
                "You must include the file generated %s into the config as resource in order to use it into your application.",
                str_replace($directory, '', $file)
            )
        );
    }
} 