<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 06/03/14
 * Time: 15:57
 */

namespace La\MessagingBundle\Command;


use La\Lib\Messaging\Message\SmartFocusMessage;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SendCommand extends ContainerAwareCommand
{

    protected $templates = array();

    /**
     * @param $service
     * @return object
     */
    public function get($service)
    {
        return $this->getContainer()->get($service);
    }

    protected function configure()
    {
        $this
            ->setName('messaging:send')
            ->setDescription("Send single email using messagging library")
            ->addOption('adapter', 'a',  InputOption::VALUE_OPTIONAL, 'swiftmailer' )
            ->addArgument('email', InputArgument::OPTIONAL, 'Email to send', null)
            ->addArgument('template', InputArgument::OPTIONAL, 'template used for sending the email', null)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dialog = $this->getHelperSet()->get('dialog');
        $mailer = $this->get('la.mailer');

        $messages = array();

        $adapter = $input->getOption('adapter') ?: $this->getContainer()->getParameter('la_messaging.default_mailer');

        $mailer
            ->using($adapter)
        ;
        $templates = array_keys($mailer->getMailerAdapter()->getTemplates());


        $email = null != $input->getArgument('email') ? $input->getArgument('email') : $dialog->ask(
            $output,
            sprintf("Fill the email address to send: "),
            null,
            null
        );


        $tpl = null !== $input->getArgument('template') ? $input->getArgument('template') : $dialog->ask(
            $output,
            sprintf("Please enter the name of a template (%s): ", implode(', ', $templates)),
            function ($answer) {
                if (!in_array($answer, $this->templates)) {
                    throw new \RunTimeException(
                        'The name of template is not declared in configuration settings'
                    );
                }
                return $tpl = $answer;
            },
            null,
            $templates
        );

        $mailer
            ->withTemplate($tpl)
        ;
        $context = $mailer->getContext();

        $output->writeln(sprintf("Fill the variables for template \"%s\"", $tpl));
        foreach ($context as $key => $var) {
            $context[$key] = $dialog->ask(
                $output,
                sprintf("%s : ", $key),
                null,
                null
            );
        }

        $count = $mailer->createAndSend( $email, $context);

        $output->writeln(sprintf("<comment>%s</comment> emails were sent.", $count));
    }
} 