<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 17/03/14
 * Time: 16:01
 */

namespace La\MessagingBundle\Event;

use La\MessagingBundle\Adapter\MailerAdapterInterface;
use Symfony\Component\EventDispatcher\Event;

class EmailEvent extends Event
{

    /**
     * @var \La\MessagingBundle\Adapter\MailerAdapterInterface|null
     */
    protected $mailerAdapter = null;

    protected $messages = array();

    protected $template = null;

    protected $response;

    protected $sent;

    function __construct(MailerAdapterInterface $mailerAdapter, $reponse = null)
    {
        $this->mailerAdapter = $mailerAdapter;

        $this->sent = count($mailerAdapter->getMessages()) - count($mailerAdapter->getFailures());
        $this->response = $reponse ?: $response = sprintf(
            "%s %s emails from %s with template \"%s\". Server responded with: \"%s\"",
            method_exists($mailerAdapter, 'getSpool') && $mailerAdapter->getSpool() && !$mailerAdapter->getSpool()->mustAutoflush() ? 'Spooled' : 'Sent',
            $this->sent,
            count($mailerAdapter->getMessages()),
            $mailerAdapter->getTemplate(),
            $this->sent == count($mailerAdapter->getMessages()) ? 'success' : 'sent with failures'
        );
    }

    /**
     * @return null
     */
    public function getMailerAdapter()
    {
        return $this->mailerAdapter;
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->mailerAdapter->getMessages();
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->mailerAdapter->getTemplate();
    }

    public function getSent()
    {
        return $this->sent;
    }

} 