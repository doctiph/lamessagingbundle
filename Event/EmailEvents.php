<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 17/03/14
 * Time: 16:03
 */

namespace La\MessagingBundle\Event;

/**
 * Class EmailEvents
 * @package La\MessagingBundle\Event
 */
final class EmailEvents {

    /**
     * Sent when emails are sent to server and this one return succesfully
     */
    const EMAIL_SENT_WITH_SUCCESS = 'la_messaging.email_sent.success';

    /**
     * Sent when emails sent is not successful
     */
    const EMAIL_SENT_WITH_ERROR = 'la_messaging.email_sent.error';

} 