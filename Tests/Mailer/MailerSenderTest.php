<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 20/03/14
 * Time: 10:28
 */

namespace La\MessagingBundle\Tests\Mailer;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MailerSenderTest extends WebTestCase
{

    /**
     * @var ContainerInterface
     */
    private $container;


    public function setUp()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        $this->container = $kernel->getContainer();
    }

    /**
     * @test
     */
    public function testGetMailerAdapter()
    {
        $mailer = $this->container->get('la.mailer');

        $this->assertInstanceOf('La\\MessagingBundle\\Adapter\\AbstractMailerAdapter', $mailer->getMailerAdapter());
    }


    /**
     * @covers MailerSender::createAndQueue
     * @covers MailerSender::flush
     * @covers MailerSender::send
     * @test
     */
    public function testCreateAndQueue()
    {
        $mailer = $this->container->get('la.mailer');

        $mailer->withTemplate('reset_password');

        $emails = array();
        for ($i = 0; $i < 3; $i++) {
            $emails[] = sprintf("ibrael%s@mailHazard.com", $i);
            $mailer->createAndQueue(sprintf("ibrael%s@mailHazard.com", $i), array(
                    'to' => 'ibrael',
                    'subject' => "test"

                ), array());
        }

        $emails[] = sprintf("ibrael");
        $mailer->createAndQueue(sprintf("ibrael"), array(), array());

        $countQueue = count($mailer->getQueue());
        $sent = $mailer->flush();

        $this->assertEquals(count($emails), $countQueue, "Checking that queue has the same quantity of emails");
        $this->assertEquals(3, $sent, "Checking emails sent");
        $this->assertEquals(1, count($mailer->getFailures()), "Checking one failure");

    }

    public function testCreateAndQueueWithSwiftmailer()
    {
        $mailer = $this->container->get('la.mailer');

        $mailer->using('swiftmailer')->withTemplate('welcome');

        $emails = array();
        for ($i = 0; $i < 3; $i++) {
            $emails[] = sprintf("ibrael%s@mailHazard.com", $i);
            $mailer->createAndQueue(
                sprintf("ibrael%s@mailHazard.com", $i),
                array(
                    'user' => array('username' => uniqid()),
                ),
                array()
            );
        }



        try{
            $mailer->createAndQueue(sprintf("ibrael"), array(
                    'user' => array('username' => uniqid()),
            ), array());
            $emails[] = sprintf("ibrael");

        }catch (\Exception $e){
           $this->assertContains('RFC', $e->getMessage(), "Checking compatibilty");
        }


        $countQueue = count($mailer->getQueue());
        $sent = $mailer->flush();

        $this->assertEquals(count($emails), $countQueue, "Checking that queue has the same quantity of emails");
        $this->assertEquals(3, $sent, "Checking emails sent");

    }
}
 