<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 28/03/14
 * Time: 11:21
 */

namespace La\MessagingBundle\Tests\Model;


use La\Lib\Messaging\Message\SmartFocusMessage;
use La\MessagingBundle\Model\SmartFocusWrapper;
use La\MessagingBundle\Model\SwiftMailerWrapper;

class SwiftMailerWrapperTest extends \PHPUnit_Framework_TestCase {

    /**
     * @covers SwiftMailerWrapper::createWrapped
     * @test
     */
    public function testCreateWrapped()
    {
        $wrapper = new SwiftMailerWrapper('toto@gmail.com', array(), array());


        $this->assertNotNull($wrapper->getWrappedMessage(), "Checking that wrapper creates internal message");

        $this->assertInstanceOf('Swift_Message', $wrapper->getWrappedMessage());

        $this->assertEquals('toto@gmail.com', $wrapper->getDestination());

        // format used by swift
        $this->assertEquals(array($wrapper->getDestination() => null ), $wrapper->getWrappedMessage()->getTo());
    }

    /**
     * @covers SwiftMailerWrapper::wrap
     * @test
     */
    public function testWrap()
    {
        $message = new SmartFocusMessage('toto@gmail.com', array());

        $wrapper = new SwiftMailerWrapper('toto@gmail.com');

        try{
            $wrapper->wrap($message);
        }catch (\Exception $e)
        {
            return;
        }
        $this->fail("No exception raised on invalid argument ");

    }
}
 