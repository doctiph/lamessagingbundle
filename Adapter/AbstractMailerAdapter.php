<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 18/03/14
 * Time: 13:25
 */

namespace La\MessagingBundle\Adapter;


abstract class AbstractMailerAdapter implements MailerAdapterInterface
{

    /**
     * @var $mailer
     */
    protected $mailer;

    protected $templates = array();

    protected $context = array();

    protected $failures = array();

    protected $priority = 0;

    /**
     * @param int $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @return object
     */
    public function getMailer()
    {
        return $this->mailer;
    }

    /**
     * {@inheritdoc}
     */
    public abstract function shouldHandle($message);

    /**
     * Specify which template to use
     * @param $templateName
     * @return mixed
     */
    public abstract function useTemplate($templateName);

    /**
     * @return mixed
     */
    public abstract function getMessages();

    /**
     * Create and send the email(s)
     *
     * @param array $to
     * @param array $context
     * @param array $parameters
     * @param array $failures
     * @return int
     */
    public function createAndSend($to = array(), $context = array(), array $parameters = array(), &$failures)
    {
        $message = $this->createMessage($to, $context, $parameters);

        $count = $this->send(is_array($message) ? $message : array($message), $this->failures) ;

        $failures = array_merge($failures, $this->failures);

        return $count;
    }

    /**
     * {@inheritdoc}
     */
    public abstract function createMessage($to, $context = array(), array $parameters = array());

    /**
     * @param array $messages
     * @param array $failures
     * @return mixed
     */
    public abstract function send(array $messages = array(), &$failures = array());

    /**
     * Store the templates used by the mailer
     *
     * @param array $templates
     * @return mixed
     */
    public function store($templates = array())
    {
        $this->templates = $templates;
    }

    /**
     * @return mixed
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @return array
     */
    public function getTemplates()
    {
        return $this->templates;
    }

    /**
     * @param array $templates
     */
    public function setTemplates($templates)
    {
        $this->templates = $templates;
    }

    /**
     * Determines if an adapter has a template available
     *
     * @param string $templateName
     * @return bool
     */
    public function hasTemplate($templateName = '')
    {
        return in_array($templateName, array_keys($this->templates));
    }


    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * {@inheritdoc}
     */
    public abstract function getName();

    /**
     * @return array
     */
    public function getFailures()
    {
        return $this->failures;
    }
}