<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 18/03/14
 * Time: 13:23
 */

namespace La\MessagingBundle\Adapter;


use La\Lib\Messaging\Mailer\Mailer;
use La\Lib\Messaging\Mailer\SmartFocusRestMailer;
use La\Lib\Messaging\Message\SmartFocusMessage;
use La\MessagingBundle\Spool\Spool;
use La\MessagingBundle\Model\SmartFocusWrapper;

class SmartFocusAdapter extends AbstractMailerAdapter
{

    protected $messages = array();
    /**
     * @var SmartFocusRestMailer
     */
    protected $mailer;

    /** @var Spool */
    protected $spool = null;

    public function __construct(Mailer $mailer, array $spool)
    {
        $this->mailer = $mailer;
        if ($spool['enabled']) {
            $this->spool = new Spool($spool['path'], $spool['autoflush']);
        }
    }

    /**
     * @return SmartFocusRestMailer
     */
    public function getMailer()
    {
        return $this->mailer;
    }

    /**
     * @return Spool
     */
    public function getSpool()
    {
        return $this->spool;
    }

//    /**
//     * {@inheritdoc}
//     */
//    public function createAndSend($to, $context = array(), array $parameters = array(), &$failures)
//    {
//        $message = $this->createMessage($to, $context, $parameters);
//
//        return $this->send(is_array($message) ? $message : array($message), $failures);
//    }

    /**
     * {@inheritdoc}
     * @return SmartFocusMessage|array
     */
    public function createMessage($to, $context = array(), array $parameters = array())
    {
        $context = array_merge($this->getContext(), $context);

        // SmartFocus HACK : If this is passed in $parameters, set in in context  because that's how SmartFocus rolls.
        foreach (array(
                     'maxLineLength',
                     'priority',
                     'date',
                     'contentType',
                     'charset',
                     'sender',
                     'subject') as $property) {
            if (isset($parameters[$property])) {
                $context[$property] = $parameters[$property];
            }
        }
        foreach (array(
                     'from',
                     'cc',
                     'bcc',
                     'replyTo',
                     'returnPath',
                 ) as $property) {
            if (isset($parameters[$property])) {
                if (is_array($parameters[$property])) {
                    if ($property == 'from') {
                        $context[$property] = array_values($parameters[$property])[0];
                    } else {
                        $context[$property] = array_keys($parameters[$property])[0];
                        $context[$property . 'Name'] = array_values($parameters[$property])[0];
                    }
                } else {
                    $context[$property] = $parameters[$property];
                }
            }
        }
        // END SmartFocus HACK

        if (is_array($to)) {
            $messages = array();
            foreach ($to as $email) {
                $messages[] = new SmartFocusWrapper($email, $context, $parameters);
            }

            return $messages;
        }

        return new SmartFocusWrapper($to, $context, $parameters);
    }

    /**
     * @param array $messages
     * @param array $failures
     * @return mixed|void
     * @throws \LogicException
     */
    public function send(array $messages = array(), &$failures = array())
    {
        $this->failures = (array)$failures;

        $this->messages = $messages;

        // checking and extracting
        $copy = array();
        foreach ($messages as $msg) {
            if (!$this->shouldHandle($msg)) {
                throw new \LogicException(sprintf(
                    "The %s type can't be handled by this adapter!",
                    get_class($msg)
                ));
            }

            if ($msg instanceof SmartFocusWrapper) {
                $copy[] = $msg->getWrappedMessage();
            } else {
                $copy[] = $msg;
            }
        }

        if ($this->spool) {
            return $this->spool->queueMessages($copy, $this->getTemplate(), $failures);
        }
        return $this->mailer->send($copy, $failures);
    }

    /**
     * determine if this the adecuate adapter for a message
     *
     * @param $message
     * @return mixed
     */
    public function shouldHandle($message)
    {
        return ($message instanceof SmartFocusMessage) || $message instanceof SmartFocusWrapper || is_string($message);
    }

    public function getName()
    {
        return 'smartfocus';
    }

    /**
     * @param $templateName
     * @return mixed|void
     * @throws \Exception
     */
    public function useTemplate($templateName)
    {
        if (!in_array($templateName, array_keys($this->templates))) {
            throw new \Exception(sprintf(
                "The template \"%s\" is not configured for the %s adapter. Maybe you have a misspelling into config file?",
                $templateName,
                get_class($this)
            ));
        }

        $this->mailer->setTemplate($this->templates[$templateName]);

        $this->context = $this->mailer->getDynVars();
    }

    public function getContext()
    {
        return $this->mailer->getDynVars();
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    public function getTemplate()
    {
        return $this->mailer->getTemplate();
    }


    public function flushSpool()
    {
        $sent = 0;
        $queue = $this->getSpool()->flushQueue();

        $process = [];
        $templates = [];
        foreach ($queue as $file => $item) {
            $process[$item['template']->getName()][$file] = $item['message'];
            $templates[$item['template']->getName()] = $item['template'];
        }
        $failures = [];

        foreach ($process as $template => $messages) {
            $file = array_keys($messages)[0];
            $this->mailer->setTemplate($templates[$template]);
            try {
                $sent += $this->mailer->send($messages, $failures);
                @unlink($file . '.sending');
            } catch (\Exception $e) {
                rename($file . '.sending', $file . '.error');
            }
        }
        return $sent;
    }
}