<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 18/03/14
 * Time: 13:12
 */

namespace La\MessagingBundle\Adapter;


use La\MessagingBundle\Model\SwiftMailerWrapper;

class SwiftMailerAdapter extends AbstractMailerAdapter
{
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    protected $twig;

    protected $currentTemplate;

    protected $messages = array();

    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig)
    {
        $this->mailer = $mailer;

        $this->twig = $twig;
    }

    /**
     * @param array $messages
     * @param array $failures
     * @return int|mixed
     * @throws \LogicException
     */
    public function send(array $messages = array(), &$failures = array())
    {
        $this->failures = array();
        $this->messages = $messages;
        $counter = 0;

        foreach ($messages as $msg) {
            if (!$this->shouldHandle($msg)) {
                throw new \LogicException(sprintf(
                    "This adapter expects messages of '%s' type, '%s' received!",
                    'Swift_Message',
                    get_class($msg)
                ));
            }

            try {
                $counter += $msg instanceof SwiftMailerWrapper ? $this->mailer->send(
                    $msg->getWrappedMessage(),
                    $this->failures
                ) : $this->mailer->send($msg, $this->failures);
            } catch (\Exception $e) {
                $this->failures[$msg->getDestination()] = $e->getMessage();
            }

        }
        return $counter;
    }

    /**
     * {@inheritdoc}
     */
    public function shouldHandle($message)
    {
        return ($message instanceof \Swift_Message) || $message instanceof SwiftMailerWrapper || is_string($message);
    }

    /**
     * @return object|\Swift_Mailer
     */
    public function getMailer()
    {
        return $this->mailer;
    }

    /**
     * {@inheritdoc}
     *
     * parameters = array(
     *      'charset',
     *      'to',
     *      'cc',
     *      'bcc',
     *      'from',
     *      'sender',
     *      'sender',
     *      'returnPath',
     *      'maxLineLength',
     *      'priority',
     *      'date',
     *      'contentType',
     *      'body',
     *      'parts',
     *      'subject',
     *      'attachments',
     *      'certificate',
     *      'signer',
     *
     *      // all the parameters from \Swift_Message
     * )
     *
     * @return \Swift_Message
     */
    public function createMessage($to, $context = array(), array $parameters = array())
    {
        $context = $this->twig->mergeGlobals($context);

        $template = $this->twig->loadTemplate($this->currentTemplate);

        $subject = isset($parameters['subject'])
            ? $parameters['subject']
            : $template->renderBlock('subject', $context);

        $textBody = isset($parameters['parts']['text'])
            ? $parameters['parts']['text']
            : $template->renderBlock('body_text', $context);

        $htmlBody = isset($parameters['body'])
            ? $parameters['body']
            : $template->renderBlock('body_html', $context);

        $expected = array(
            'body' => $htmlBody,
            'parts' => array(
                'text/plain' => $textBody
            ),
            'subject' => $subject
        );

        // adding others
        foreach (array(
                     'to',
                     'cc',
                     'bcc',
                     'from',
                     'returnPath',
                     'maxLineLength',
                     'priority',
                     'date',
                     'replyTo',
                     'contentType',
                     'charset',
                     'sender',
                 ) as $property) {
            if (isset($parameters[$property])) {
                $expected[$property] = $parameters[$property];
            }
        }

        // attachments
        if (isset($parameters['attachments'])) {
            foreach ($parameters['attachments'] as $myme_type => $uri) {
                if (!is_numeric($myme_type)) {
                    $expected['attachments'][] = \Swift_Attachment::fromPath($uri, $myme_type);
                } else {
                    $expected['attachments'][] = \Swift_Attachment::fromPath($uri);
                }
            }
        }

        // sign and encrypt
        if (isset($parameters['certificate'])) {
            $smimeSigner = \Swift_Signers_SMimeSigner::newInstance();
            if (is_array($parameters['certificate'])) {
                $privateKey = null;
                $passphrase = null;
                $sign = null;

                if (isset($parameters['certificate']['encrypt'])) {
                    $smimeSigner->setEncryptCertificate($parameters['certificate']['encrypt']);
                }

                if (isset($parameters['certificate']['sign'])) {
                    $sign = $parameters['certificate']['sign'];
                }

                if (isset($parameters['certificate']['privateKey'])) {
                    $privateKey = $parameters['certificate']['privateKey'];
                }

                if (isset($parameters['certificate']['passphrase'])) {
                    $passphrase = $parameters['certificate']['passphrase'];
                }

                if (!$sign) {
                    throw new \Exception('If you decide to use a digital sign for message, you must specify the signing certificate');
                }
                if ($sign && !$privateKey) {
                    throw new \Exception('You must specify the private key for signing certificate');
                }

                if ($passphrase != null) {
                    $smimeSigner->setSignCertificate($sign, $privateKey, $passphrase);
                } else {
                    $smimeSigner->setSignCertificate($sign, $privateKey);
                }
            } else {
                throw new \Exception('You must provide the "certificate" parameter in the array format as array("privateKey" => "", "sign" => "", "passphrase" => "", "encrypt" => "")');
            }
            $expected['signer'] = $smimeSigner;
        }


        return new SwiftMailerWrapper($to, $context, $expected);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'swiftmailer';
    }

    /**
     * Specify which template to use
     * @param $templateName
     * @return mixed
     */
    public function useTemplate($templateName)
    {
        $this->currentTemplate = $this->templates[$templateName];
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->currentTemplate;
    }


}