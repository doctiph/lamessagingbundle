<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 18/03/14
 * Time: 13:19
 */

namespace La\MessagingBundle\Adapter;


/**
 * Interface for create adapters for libraries
 *
 * Interface MailerAdapterInterface
 * @package La\MessagingBundle\Adapter
 */
interface MailerAdapterInterface
{

    /**
     * @return int
     */
    public function getPriority();

    /**
     * @param array $messages
     * @param array $failures
     * @return mixed
     */
    public function send(array $messages = array(), &$failures);

    /**
     * @return object
     */
    public function getMailer();

    /**
     * Creates an instance of the message to send.
     *
     * @param $to
     *
     */
    public function createMessage($to);

    /**
     * Shortcuts for create and send messages
     * @param $to
     * @param array $context
     * @param array $parameters
     * @param array $failures
     * @return mixed
     */
    public function createAndSend($to, $context = array(), array $parameters = array(), &$failures);

    /**
     * Determine if this the adecuate adapter for a message
     *
     * @param $message
     * @return mixed
     */
    public function shouldHandle($message);

    /**
     * @return string
     */
    public function getName();

    /**
     * Store the templates used by the mailer
     *
     * @param array $templates
     * @return mixed
     */
    public function store($templates = array());
} 