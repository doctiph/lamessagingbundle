Implémentation MessagingBundle dans le cadre du UserBundle
==========================================================

# Installation

### Pour l'exemple nous utiliserons le site elle.fr

## Update composer.json :

    "repositories": [
#....
        {
            "type": "git",
            "url": "git@git01.in.ladtech.fr:LaMessagingBundle"
        },
        {
            "type": "git",
            "url": "git@git01.in.ladtech.fr:LaLibMessaging"
        },
    ],
#....
    "require": {
        "la/libmessaging": "dev-master",
        "la/messagingbundle": "dev-master"
    },

## Update /app/AppKernel.php

     public function registerBundles()
     {
         $bundles = array(
             // ....
             new La\MessagingBundle\LaMessagingBundle(),
             // ....
         );
     }

# Import des fichiers de configuration

# Configuration

### app/config/la_config.yml

    la_messaging:
        mailer:   swiftmailer

Ceci est la configuration minimale, pour utiliser le mailer par defaut. Si vous utilisez un mailer personnalisé comme smartfocus,
Merci de vous reporter à la rubrique dédiée dans /Resources/doc/index.md