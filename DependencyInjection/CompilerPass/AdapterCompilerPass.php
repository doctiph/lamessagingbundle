<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 19/03/14
 * Time: 16:19
 */

namespace La\MessagingBundle\DependencyInjection\CompilerPass;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class AdapterCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('la.mailer')) {
            return;
        }

        $definition = $container->getDefinition(
            'la.mailer'
        );

        $taggedServices = $container->findTaggedServiceIds(
            'la_messaging.adapter'
        );


        foreach ($taggedServices as $id => $tagAttributes) {

            foreach($tagAttributes as $attributes)
            $definition->addMethodCall(
                'addAdapter',
                array(new Reference($id), $attributes['priority'])
            );
        }

        $definition->addMethodCall('using',
            array($container->getParameter('la_messaging.default_mailer')));
    }
}