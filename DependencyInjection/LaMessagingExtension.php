<?php

namespace La\MessagingBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\LogicException;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class LaMessagingExtension extends Extension
{


    public function getAlias()
    {
        return 'la_messaging';
    }

    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();

        $tpl = array();
        $configs = array_map(
            function ($conf) use (&$tpl) {
                if (isset($conf['templates'])) {
                    $tpl = array_merge($tpl, $conf['templates']);
                    $conf['templates'] = $tpl;
                }
                return $conf;

            },
            $configs
        );

        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.xml');

        $container->setParameter('la_messaging.config', $config);
        $container->setParameter('la_messaging.default_mailer', $config['mailer']);

        // smartfocus
        if (isset($config['swiftmailer']) || $config['mailer'] == 'swiftmailer') {
            $loader->load('swiftmailer.xml');
        }
        if (isset($config['smartfocus']) || $config['mailer'] == 'smartfocus') {
            $loader->load('smartfocus.xml');
        }

        if (isset($config['templates'])) {
            $templates = (array)$config['templates'];
            $adapter_templates = array();
            foreach ($templates as $name => $adapters) {
                if (count($adapters)) {
                    foreach ($adapters as $adapter => $realTemplate) {
                        $adapter_templates[$adapter][$name] = $realTemplate;
                    }
                }
            }
            $container->setParameter("la_messaging.templates", $adapter_templates);
        }


        // smartfocus
        if (isset($config['smartfocus'])) {

            if(is_null($config['smartfocus']['spool']['path'])){
                $config['smartfocus']['spool']['path'] = sprintf('%s/spool/smartfocus',$container->getParameter('kernel.root_dir'));
            }
            // verifying that templates declared globally really exist in smartfocus config
            if (isset($config['smartfocus']['templates']) && count($config['smartfocus']['templates'])) {

                if (isset($adapter_templates['smartfocus']) && count($adapter_templates['smartfocus'])) {
                    foreach ($adapter_templates['smartfocus'] as $templateName) {
                        if (!isset($config['smartfocus']['templates'][$templateName])) {
                            throw new LogicException(sprintf(
                                "The template %s is declared into the global configuration
                                as an smartfocus template, but is not declared into smartfocus config. maybe it doesn't exist? Availables actually: \"%s\"",
                                $templateName,
                                implode('", "', array_keys($config['smartfocus']['templates']))
                            ));
                        }
                    }
                }
            }

            $container->setParameter("la_messaging.smartfocus.url", $config['smartfocus']['url']);
            $container->setParameter("la_messaging.smartfocus.batch", $config['smartfocus']['batch']);
            $container->setParameter("la_messaging.smartfocus.spool", $config['smartfocus']['spool']);

            $container->setParameter("la_messaging.smartfocus.templates", $config['smartfocus']['templates']);
            $container->setParameter('la_messaging.smartfocus.config', $config['smartfocus']);


            if (extension_loaded('soap')) {
                $container->setParameter(
                    'la_messaging.mailer.smartfocus.class',
                    'La\Lib\Messaging\Mailer\SmartFocusSoapMailer'
                );
            } elseif (extension_loaded('curl')) {
                $container->setParameter(
                    'la_messaging.mailer.smartfocus.class',
                    'La\Lib\Messaging\Mailer\SmartFocusRestMailer'
                );
            } else {
                $container->setParameter(
                    'la_messaging.mailer.smartfocus.class',
                    'La\Lib\Messaging\Mailer\SmartFocusFGetMailer'
                );
            }
        }
    }

    /**
     * @param $loader
     * @param $container
     * @param $config
     * @param $adapter_templates
     * @throws \Symfony\Component\DependencyInjection\Exception\LogicException
     */
    private function addSmartFocusServices($loader, $container, $config, $adapter_templates)
    {


        // verifying that templates declared globally really exist in smartfocus config
        if (isset($adapter_templates['smartfocus']) && count($adapter_templates['smartfocus'])) {
            foreach ($adapter_templates['smartfocus'] as $templateName) {
                if (!isset($config['smartfocus']['templates'][$templateName])) {
                    throw new LogicException(sprintf(
                        "The template %s is declared into the global configuration
                        as an smartfocus template, but is not declared into smartfocus config. maybe it doesn't exist? Availables actually: \"%s\"",
                        $templateName,
                        implode('", "', array_keys($config['smartfocus']['templates']))
                    ));
                }
            }
        }

        $container->setParameter("la_messaging.smartfocus.url", $config['smartfocus']['url']);
        $container->setParameter("la_messaging.smartfocus.batch", $config['smartfocus']['batch']);

        $container->setParameter("la_messaging.smartfocus.templates", $config['smartfocus']['templates']);
        $container->setParameter('la_messaging.smartfocus.config', $config['smartfocus']);


        if (extension_loaded('soap')) {
            $container->setParameter(
                'la_messaging.mailer.smartfocus.class',
                'La\Lib\Messaging\Mailer\SmartFocusSoapMailer'
            );
        } elseif (extension_loaded('curl')) {
            $container->setParameter(
                'la_messaging.mailer.smartfocus.class',
                'La\Lib\Messaging\Mailer\SmartFocusRestMailer'
            );
        } else {
            $container->setParameter(
                'la_messaging.mailer.smartfocus.class',
                'La\Lib\Messaging\Mailer\SmartFocusFGetMailer'
            );
        }
    }


}
