<?php

namespace La\MessagingBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('la_messaging');


        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        $rootNode
            ->children()
                ->scalarNode('mailer')
                    ->isRequired()
                    ->cannotBeEmpty()
                    ->defaultValue('mailer')
                ->end()
            ->end()
            ->children()
                ->variableNode('templates')
                    ->validate()
                        ->ifTrue(function($templates){
                            return  !is_null($templates) && !is_array($templates);
                        })
                        ->thenInvalid("The key templates must be an array.")
                ->end()
            ->end()
        ;


        // add smartfocus configuration section
        $this->addSmartFocusSection($rootNode);

        return $treeBuilder;
    }


    /**
     * Add the configuration section for smartfocus client
     *
     * @param ArrayNodeDefinition $root
     */
    public function addSmartFocusSection(ArrayNodeDefinition $root)
    {
        $root
            ->children()
                ->arrayNode('smartfocus')
                    ->children()
                        ->scalarNode('server')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                    ->end()
                    ->children()
                        ->scalarNode('login')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                    ->end()

                    ->children()
                        ->scalarNode('pwd')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                    ->end()

                    ->children()
                        ->scalarNode('key')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                    ->end()

                    ->children()
                        ->scalarNode('url')
                            ->defaultValue("api.notificationmessaging.com")
                            ->cannotBeEmpty()
                        ->end()
                    ->end()

                    ->children()
                        ->arrayNode('spool')
                            ->canBeEnabled()
                            ->children()
                                ->scalarNode('autoflush')
                                    ->defaultValue(false)
                                ->end()
                                ->scalarNode('path')
                                    ->defaultValue(null)
                                ->end()
                            ->end()
                        ->end()
                    ->end()

                    ->children()
                        ->booleanNode('batch')->defaultTrue()->end()
                    ->end()
                    ->children()
                        ->booleanNode('loggable')->defaultFalse()->end()
                    ->end()
                    ->children()
                        ->arrayNode('templates')
                            ->useAttributeAsKey('name')
                            ->prototype('array')
                                ->children()
                                    ->scalarNode('name')->end()
                                    ->scalarNode('random')->cannotBeEmpty()->end()
                                    ->scalarNode('encrypt')->cannotBeEmpty()->end()
                                    ->arrayNode('dyn')
                                        ->prototype('scalar')
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
