<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 17/03/14
 * Time: 16:20
 */

namespace La\MessagingBundle\EventDispatcher;

use La\MessagingBundle\Adapter\MailerAdapterInterface;
use La\MessagingBundle\DataCollector\MessagingDataCollector;
use La\MessagingBundle\Event\EmailEvent;
use La\MessagingBundle\Event\EmailEvents;
use La\Lib\Messaging\Mailer\Mailer;
use La\MessagingBundle\Model\SwiftMailerWrapper;
use Psr\Log\LoggerInterface;

class EmailEventListener
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    protected $collector;

    public function __construct(LoggerInterface $logger, MessagingDataCollector $collector)
    {
        $this->logger = $logger;

        $this->collector = $collector;
    }

    public static function getSubscribedEvents()
    {
        return array(
            EmailEvents::EMAIL_SENT_WITH_SUCCESS => 'onEmailSentWithSuccess',
            EmailEvents::EMAIL_SENT_WITH_ERROR => 'onEmailSentWithError'
        );
    }

    /**
     * Create logs on success
     *
     * @param EmailEvent $event
     */
    public function onEmailSentWithSuccess(EmailEvent $event)
    {
        $mailer = $event->getMailerAdapter();
        $messages = $mailer->getMessages();

        $sent = count($messages) - count($mailer->getFailures());

        if (method_exists($mailer, 'flushSpool') && $mailer->getSpool() && $mailer->getSpool()->mustAutoflush()) {
            $sent += $mailer->flushSpool();
        }

        $response = sprintf(
            "Sent %s emails from %s with template \"%s\" using \"%s\" adapter. Server responded: \"%s\"",
            $sent,
            count($messages),
            $mailer->getTemplate(),
            $event->getMailerAdapter(),
            $sent < count($messages) ? 'sent with failures' : 'success'
        );
        if ($this->logger != null) {
            $this->logger->info($response, $mailer->getFailures());
        }

        if ($this->collector != null) {
            $this->collector->loadFromEvent($event);
        }
    }

    public function onEmailSentWithError(EmailEvent $event)
    {
        $mailer = $event->getMailerAdapter();
        $messages = $mailer->getMessages();

        $sent = count($messages) - count($mailer->getFailures());
        $response = sprintf(
            "Trying to send %s emails with template \"%s\" using \"%s\" adapter. Server responded: \"%s\"",
            $sent,
            $mailer->getTemplate(),
            $event->getMailerAdapter(),
            'Error!'
        );
        if ($this->logger != null) {
            $this->logger->error($response, $mailer->getMessages());
        }

        if ($this->collector != null) {
            $this->collector->loadFromEvent($event);
        }
    }

}