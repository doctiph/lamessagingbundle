<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 17/03/14
 * Time: 14:13
 */

namespace La\MessagingBundle\Log;


use Psr\Log\LoggerTrait;

class Logger {

    use LoggerTrait
    /**
     * The log contents.
     *
     * @var array
     */
    private $_log = array();

    /**
     * Max size of the log.
     *
     * @var int
     */
    private $_size = 0;

    /**
     * Create a new ArrayLogger with a maximum of $size entries.
     *
     * @var int $size
     */
    public function __construct($size = 50)
    {
        $this->_size = $size;
    }

    /**
     * Add a log entry.
     *
     * @param string $entry
     */
    public function add($entry)
    {
        $this->_log[] = $entry;
        while (count($this->_log) > $this->_size) {
            array_shift($this->_log);
        }
    }

    /**
     * Clear the log contents.
     */
    public function clear()
    {
        $this->_log = array();
    }

    /**
     * Get this log as a string.
     *
     * @return string
     */
    public function dump()
    {
        return implode(PHP_EOL, $this->_log);
    }

    public function getLogs()
    {
        return $this->_log;
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return null
     */
    public function log($level, $message, array $context = array())
    {
        $this->add(sprintf("%s: %s %s", $level, $message));
    }
}