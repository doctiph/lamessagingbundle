<?php
/**
 * Created by PhpStorm.
 * User: Ibrael Espinosa com-i.espinosa@lagardere-active.com
 * Date: 12/03/14
 * Time: 11:30
 */

namespace La\MessagingBundle\DataCollector;

;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;
use La\MessagingBundle\Event\EmailEvent;


class MessagingDataCollector extends DataCollector
{
    /**
     * @var array
     */
    private $config;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var EmailEvent[]
     */
    private $events = array();

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->config = $this->container->getParameter('la_messaging.config');

        $this->data = array(
            'logs' => array(),
            'total' => 0,
            'adapters' => array(),
            'messages' => array(),
            'config' => $this->config
        );
    }

    /**
     * {@inheritdoc}
     * @param Request $request
     * @param Response $response
     * @param \Exception $exception
     */
    public function collect(Request $request, Response $response, \Exception $exception = null)
    {

        foreach ($this->events as $event) {
            $this->data['logs'][] = $event->getResponse();

            $adapter = $event->getMailerAdapter()->getName();

            if (!isset($this->data['adapters'][$adapter])) {
                $this->data['adapters'][$adapter] = 0;
            }
            if (!isset($this->data['messages'][$adapter])) {
                $this->data['messages'][$adapter] = array();
            }

            $this->data['adapters'][$adapter] = count($event->getMessages());
            $this->data['total'] += $this->data['adapters'][$adapter];

            foreach ($event->getMessages() as $msg) {
                $this->data['messages'][$adapter][] = array(
                    'email' => $msg->getDestination(),
                    'context' => json_encode($msg->getContext()),
                    'sent_date' => $msg->getSentDate(),
                    'is_spooled' => $this->isSpoolEnabled($adapter),
                    'template' => $event->getTemplate()
                );
            }
        }

    }

    public function isSpoolEnabled($adapter)
    {
        switch($adapter){
            case 'swiftmailer':
                return $this->container->getParameter('swiftmailer.mailer.default.spool.enabled');
            case 'smartfocus':
                return $this->config['smartfocus']['spool']['enabled'] && !$this->config['smartfocus']['spool']['autoflush'];
            default:
                return false;
        }
    }

    /**
     * get all templates configuration to expose
     *
     * @return array
     */
    public function getTemplates()
    {
        $templates = $this->data['config']['templates'];

        return $templates;
    }


    /**
     * Get the real template for an adapter
     *
     * @param $template
     * @param $adapter
     * @return null
     */
    public function getRealTemplateForAdapter($template, $adapter)
    {
        if (isset($this->data['config']['templates'][$template][$adapter])) {
            return $this->data['config']['templates'][$template][$adapter];
        }
        return null;
    }

    /**
     * Expose configuration
     * @return array
     */
    public function getConfig()
    {
        return $this->data['config'];
    }

    /**
     * Expose messages
     * @return mixed
     */
    public function getMessages()
    {
        return $this->data['messages'];
    }

    /**
     * Get the messages sent by an adapter
     * @param $name
     * @return mixed
     */
    public function getMessagesForAdapter($name)
    {
        return $this->data['messages'][$name];
    }

    /**
     * get all the adapters who have done interaction during this request
     * @return array
     */
    public function getAdapters()
    {
        return $this->data['adapters'];
    }

    /**
     * get the logs responses
     * @return mixed
     */
    public function getLogs()
    {
        return $this->data['logs'];
    }

    /**
     * Load information from the emailevent
     * @param EmailEvent $event
     */
    public function loadFromEvent(EmailEvent $event)
    {
        $this->events[] = $event;
    }

    /**
     * Get the total amount of messages sent
     * @return int
     */
    public function getMessageCount()
    {
        return $this->data['total'];
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'messaging';
    }


} 